// Convert between numeric, string, and Boolean types
package main 

import (
	"fmt"
	"strconv"
	)

func main(){
	age := 29
	marsAge := float64(age)
	marsDays := 687.0
	earthDays := 365.2425
	marsAge = marsAge * earthDays / marsDays
	fmt.Println("I am", marsAge, "years old on Mars.")
	fmt.Println(int(marsAge))

	var bh float64 = 32770
	var h = int16(bh)
	fmt.Println(h) // -32766

	var pi rune = 960
	var alpha rune = 940
	var omega rune = 969
	var bang byte = 33
	fmt.Print(string(pi), string(alpha), string(omega), string(bang))

	countdown := 9
	str := fmt.Sprintf("\nLaunch in T minus %v seconds.", countdown)
	fmt.Println(str)

	countdown1, err := strconv.Atoi("10")
	if err != nil {
	    // oh no, something went wrong
	}
	fmt.Println(countdown1 - 1)

	// Converting a string to a Boolean
	yesNo := "no"
	launch := (yesNo == "yes")
	fmt.Println("Ready for launch:", launch) //  Booleans in Go don’t have a numeric equivalent.

}